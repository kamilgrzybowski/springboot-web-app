package com.kamil.demo.bootstrap;

import com.kamil.demo.model.Author;
import com.kamil.demo.model.Book;
import com.kamil.demo.repository.AuthorRepository;
import com.kamil.demo.repository.BookRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Author eric = new Author("Eric", "Evans");
        Author rod = new Author("Rod", "Johnson");
        Book ddd = new Book("Domain Driven Design", "1234","Harper Collins");
        Book noEJB = new Book("J2EE Development without EJB", "2344","Worx");

        //adds ddd to erics books
        eric.getBooks().add(ddd);
        //adds eric to ddd authors
        ddd.getAuthors().add(eric);

        rod.getBooks().add(noEJB);
        noEJB.getAuthors().add(rod);

        authorRepository.save(eric);
        authorRepository.save(rod);

        bookRepository.save(ddd);
        bookRepository.save(noEJB);

    }
}
